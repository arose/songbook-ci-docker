FROM texlive/texlive:latest

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin

# install prerequisites
RUN    apt-get update \
    && apt-get install -y --no-install-recommends \
           apt-transport-https \
           bzip2 \
           ca-certificates \
           curl \
           dpkg-dev \
           dirmngr \
           gcc \
           ghostscript \
           gnupg \
           gosu \
           libc6-dev \
           libssl-dev \
           make \
           netbase \
           patch \
           perl \
           software-properties-common \
           tzdata \
           wget \
           xz-utils \
           zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

# install powershell and cleanup apt package cache
RUN    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --yes --dearmor --output /usr/share/keyrings/microsoft.gpg \
    && sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft.gpg] https://packages.microsoft.com/repos/microsoft-debian-bullseye-prod bullseye main" > /etc/apt/sources.list.d/microsoft.list' \
    && apt-get update \
    && apt-get install -y --no-install-recommends powershell \
    && rm -rf /var/lib/apt/lists/*

# setup chordpro
RUN cpan install chordpro
